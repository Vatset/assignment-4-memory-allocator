    //
// Created by Vatset Masha on 20.01.2023.
//

#ifndef MEMORY_ALLOCATOR_TESTS_H
#define MEMORY_ALLOCATOR_TESTS_H

void tests(void);

void mem_alloc(void);

void free_one_block(void);

void free_two_blocks(void);

void new_mem_region_expands(void);

void old_mem_region_cannot_be_expanded(void);


#endif //MEMORY_ALLOCATOR_TESTS_H
