//
// Created by Vatset Masha on 20.01.2023.
//

#include "mem.h"
#include "mem_internals.h"
#include "test.h"
#include <stdio.h>



#define HEAP_SIZE 4096
#define BLOCK_SIZE 128


static void destroy_heap(void* heap, size_t heap_size) {
    munmap(heap, size_from_capacity((block_capacity) {.bytes = heap_size}).bytes);
}


static struct block_header *get_header(void *ptr) {
    return (struct block_header *) ((uint8_t *) ptr - offsetof(struct block_header, contents));
}


void mem_alloc(void) {
    printf("Test <Normal successful memory allocation> started\n");
    void* heap = heap_init(HEAP_SIZE); //8192
    debug_heap(stdout, heap);

    void* data = _malloc(HEAP_SIZE/2);
    debug_heap(stdout, heap);

    if (!heap){
        return;
    }
    if (!data) {
        fprintf(stderr,"%s", "Test <Normal successful memory allocation> failed");
        destroy_heap(heap, HEAP_SIZE);
        return;
    }

    _free(data);
    debug_heap(stdout, heap);

    destroy_heap(heap, HEAP_SIZE);
    printf("Test <Normal successful memory allocation> passed\n");
}

void free_one_block(void) {
    printf("Test <Freeing one block from several allocated ones> started\n");
    void* heap = heap_init(HEAP_SIZE);
    debug_heap(stdout, heap);

    void* malloc1 = _malloc(128);
    void* malloc2 = _malloc(128);
    debug_heap(stdout, heap);
    if (!heap || !malloc1 || !malloc2) {
        fprintf(stderr,"%s", "Test <Freeing one block from several allocated ones> failed");
        return;
    }

    _free(malloc1);
    debug_heap(stdout, heap);
    destroy_heap(heap, HEAP_SIZE);
    printf("Test <Freeing one block from several allocated ones> passed\n");
}

void free_two_blocks(void) {
    printf("Test <Freeing two blocks from several allocated ones> started\n");
    void* heap = heap_init(HEAP_SIZE);
    debug_heap(stdout, heap);

    void* malloc1 = _malloc(BLOCK_SIZE);
    void* malloc2 = _malloc(BLOCK_SIZE);
    void* malloc3 = _malloc(BLOCK_SIZE);
    debug_heap(stdout, heap);

    if (!heap || !malloc1 || !malloc2 || !malloc3) {
        fprintf(stderr,"%s", "Test <Freeing two blocks from several allocated ones> failed");
        return;
    }

    _free(malloc1);
    _free(malloc2);
    debug_heap(stdout, heap);
    destroy_heap(heap, HEAP_SIZE);
    printf("Test <Freeing two blocks from several allocated ones> passed\n");
}

void new_mem_region_expands(void) {
    printf("Test <The memory is over, the new memory region expands the old one> started\n");
    void* heap = heap_init(HEAP_SIZE);
    debug_heap(stdout, heap);

    void* data = _malloc(2 * HEAP_SIZE);
    if (!data) {
        destroy_heap(heap, HEAP_SIZE);
        return;
    }


    struct block_header* header = (struct block_header*) heap;
    if (header->capacity.bytes < HEAP_SIZE * 2) {
        destroy_heap(heap, size_from_capacity(header->capacity).bytes);
        fprintf(stderr,"%s", "Test <The memory is over, the new memory region expands the old one> failed");
        return;
    }

    _free(data);
    destroy_heap(heap, HEAP_SIZE);
    printf("Test <The memory is over, the new memory region expands the old one> passed\n");
}

void old_mem_region_cannot_be_expanded(void) {
    printf("Test <Memory has run out, the old memory region cannot be expanded due to another allocated address range> started\n");
    void* heap = heap_init(HEAP_SIZE);
    debug_heap(stdout, heap);


    size_t dedicated_range = 128;
    void* barrier = mmap(heap + HEAP_SIZE, dedicated_range, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | MAP_FIXED, -1, 0);


    void* alloc = _malloc(HEAP_SIZE*2);
    if (!alloc || !heap) {
        fprintf(stderr,"%s", "Test <Memory has run out, the old memory region cannot be expanded due to another allocated address range> failed");
        return;
    }

    struct block_header* header = get_header(alloc);
    if (!header->is_free || header->next->is_free) {
        fprintf(stderr,"%s", "Test <Memory has run out, the old memory region cannot be expanded due to another allocated address range> failed");
        return;
    }

    _free(alloc);

    destroy_heap(heap, size_from_capacity(header->capacity).bytes);
    munmap(barrier, dedicated_range);
    printf("Test <Memory has run out, the old memory region cannot be expanded due to another allocated address range> passed\n");
}

void (*test_functions[])(void) = {
        mem_alloc,
        free_one_block,
        free_two_blocks,
        new_mem_region_expands,
        old_mem_region_cannot_be_expanded,
};

void tests(void){
    for (int i = 0; i < sizeof(test_functions)/sizeof(test_functions[0]); i++) {
        test_functions[i]();
    }
}
